!
!    FraME (Fragment-based Multiscale Embedding) is a software library that
!    provides versatile embedding capability to quantum chemistry codes.
!
!    Copyright (C) 2018 Jógvan Magnus Haugaard Olsen,
!                       Karen Oda Hjorth Dundas, and
!                       Magnus Ringholm.
!
!    This file is part of FraME.
!
!    FraME is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published
!    by the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    FraME is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with FraME.  If not, see <https://www.gnu.org/licenses/>.
!
!    Contact details:
!    Jógvan Magnus Haugaard Olsen (foeroyingur@gmail.com)
!

!> TODO add electronic_field and field_integrals to avoid "field = - field"
module frame_callback_procedures

    use frame_parameters
    use frame_matrix
    use frame_tensors

    implicit none

    private

    public :: set_electronic_potential
    public :: set_potential_integrals
    public :: electronic_potential
    public :: potential_integrals
    public :: electronic_potential_interface
    public :: potential_integrals_interface

    procedure(electronic_potential_interface), pointer :: external_electronic_potential => null()
    procedure(potential_integrals_interface), pointer :: external_potential_integrals => null()

    interface
        subroutine electronic_potential_interface(potentials, pot_deriv_order, density_matrices, coordinates, &
                                                nuclear_coordinates, nuclear_deriv_order, error_code)
#ifdef USE_INT64
            use iso_fortran_env, only: dp => REAL64, ip => INT64
#else
            use iso_fortran_env, only: dp => REAL64, ip => INT32
#endif
            use qcmatrix_f, only: QcMat
            !> potentials from electrons on set of coordinates
            real(dp), dimension(:,:,:,:), intent(out) :: potentials
            !> order of the potential derivative
            integer(ip), intent(in) :: pot_deriv_order
            !> input electron density matrices
            type(QcMat), dimension(:), intent(in) :: density_matrices
            !> coordinates of the sites on which the electron potential is calculated
            real(dp), dimension(:,:), intent(in) :: coordinates
            !> nuclear coordinates
            real(dp), dimension(:,:), intent(in) :: nuclear_coordinates
            !> order of the derivative with respect to nuclear coordinates
            integer(ip), intent(in) :: nuclear_deriv_order
            !> error code (0 if successful)
            integer(ip), intent(out) :: error_code
        end subroutine electronic_potential_interface
    end interface

    interface
        subroutine potential_integrals_interface(integrals, pot_deriv_order, multipoles, multipole_origins, &
                                                 min_multipole_order, max_multipole_order, &
                                                 nuclear_coordinates, nuclear_deriv_order, error_code)
#ifdef USE_INT64
            use iso_fortran_env, only: dp => REAL64, ip => INT64
#else
            use iso_fortran_env, only: dp => REAL64, ip => INT32
#endif
            use qcmatrix_f, only: QcMat
            !> integral matrices describing the potential from multipoles on electrons
            type(QcMat), dimension(:,:,:), intent(inout) :: integrals
            !> order of the potential derivative
            integer(ip), intent(in) :: pot_deriv_order
            !> sets of multipoles
            real(dp), dimension(:,:,:), intent(in) :: multipoles
            !> multipole origins
            real(dp), dimension(:,:), intent(in) :: multipole_origins
            !> minimum multipole rank
            integer(ip), intent(in) :: min_multipole_order
            !> maximum multipole rank
            integer(ip), intent(in) :: max_multipole_order
            !> nuclear coordinates
            real(dp), dimension(:,:), intent(in) :: nuclear_coordinates
            !> order of the derivative with respect to nuclear coordinates
            integer(ip), intent(in) :: nuclear_deriv_order
            !> error code (0 if successful)
            integer(ip), intent(out) :: error_code
        end subroutine potential_integrals_interface
    end interface

contains


subroutine set_electronic_potential(electronic_potential_procedure)

    procedure(electronic_potential_interface) :: electronic_potential_procedure

    external_electronic_potential => electronic_potential_procedure

end subroutine


subroutine set_potential_integrals(potential_integrals_procedure)

    procedure(potential_integrals_interface) :: potential_integrals_procedure

    external_potential_integrals => potential_integrals_procedure

end subroutine


subroutine electronic_potential(potentials, pot_deriv_order, density_matrices, coordinates, &
                              nuclear_coordinates, nuclear_deriv_order, error_code)

    !> potentials from electrons on set of coordinates
    real(dp), dimension(:,:,:,:), intent(out) :: potentials
    !> order of the potential derivative
    integer(ip), intent(in) :: pot_deriv_order
    !> input electron density matrices
    type(matrix_type), dimension(:), intent(in) :: density_matrices
    !> coordinates of the sites on which the electron potential is calculated
    real(dp), dimension(:,:), intent(in) :: coordinates
    !> nuclear coordinates
    real(dp), dimension(:,:), intent(in) :: nuclear_coordinates
    !> order of the derivative with respect to nuclear coordinates
    integer(ip), intent(in) :: nuclear_deriv_order
    !> error code (0 if successful)
    integer(ip), intent(out) :: error_code

    integer(ip) :: num_dens
    integer(ip) :: num_centers
    integer(ip) :: num_comps
    integer(ip) :: num_nuclei
    integer(ip) :: num_nuclear_comps

    error_code = 0

    num_dens = size(density_matrices)
    num_centers = size(coordinates, 2)
    num_comps = get_tensor_size(pot_deriv_order)
    num_nuclei = size(nuclear_coordinates, 2)
    num_nuclear_comps = int(binomial(3*num_nuclei+nuclear_deriv_order-1, nuclear_deriv_order), ip)

    if (size(potentials, 1) /= num_comps) then
        error_code = -1
        return
    end if

    if (size(potentials, 2) /= num_nuclear_comps) then
        error_code = -1
        return
    end if

    if (size(potentials, 3) /= num_dens) then
        error_code = -1
        return
    end if

    if (size(potentials, 4) /= num_centers) then
        error_code = -1
        return
    end if

    if (size(coordinates, 1) /= 3) then
        error_code = -1
        return
    end if

    if (size(nuclear_coordinates, 1) /= 3) then
        error_code = -1
        return
    end if

    call external_electronic_potential(potentials, pot_deriv_order, density_matrices, coordinates, &
                                     nuclear_coordinates, nuclear_deriv_order, error_code)

    if (error_code /= 0) then
        return
    end if

end subroutine electronic_potential


subroutine potential_integrals(integrals, pot_deriv_order, multipoles, multipole_origins, &
                               min_multipole_order, max_multipole_order, &
                               nuclear_coordinates, nuclear_deriv_order, error_code)

    !> potential integral matrices contracted with multipoles describing the potential
    !> from multipoles on electrons
    type(matrix_type), dimension(:,:,:), intent(inout) :: integrals
    !> order of the potential derivative
    integer(ip), intent(in) :: pot_deriv_order
    !> sets of multipoles
    real(dp), dimension(:,:,:), intent(in) :: multipoles
    !> multipole origins
    real(dp), dimension(:,:), intent(in) :: multipole_origins
    !> minimum multipole rank
    integer(ip), intent(in) :: min_multipole_order
    !> maximum multipole rank
    integer(ip), intent(in) :: max_multipole_order
    !> nuclear coordinates
    real(dp), dimension(:,:), intent(in) :: nuclear_coordinates
    !> order of the derivative with respect to nuclear coordinates
    integer(ip), intent(in) :: nuclear_deriv_order
    !> error code (0 if successful)
    integer(ip), intent(out) :: error_code

    integer(ip) :: num_comps
    integer(ip) :: num_sets
    integer(ip) :: num_sites
    integer(ip) :: num_mul_comps
    integer(ip) :: num_nuclei
    integer(ip) :: num_nuclear_comps

    error_code = 0

    num_comps = get_tensor_size(pot_deriv_order)
    num_sets = size(multipoles, 2)
    num_sites = size(multipoles, 3)
    num_mul_comps = get_polytensor_size(max_multipole_order) - get_polytensor_size(min_multipole_order-1)
    num_nuclei = size(nuclear_coordinates, 2)
    num_nuclear_comps = int(binomial(3*num_nuclei+nuclear_deriv_order-1, nuclear_deriv_order), ip)

    if (size(integrals, 1) /= num_comps) then
        error_code = -1
        return
    end if

    if (size(integrals, 2) /= num_nuclear_comps) then
        error_code = -1
        return
    end if

    if (size(integrals, 3) /= num_sets) then
        error_code = -1
        return
    end if

    if (size(multipoles, 1) /= num_mul_comps) then
        error_code = -1
        return
    end if

    if (size(multipole_origins, 1) /= 3) then
        error_code = -1
        return
    end if

    if (size(multipole_origins, 2) /= num_sites) then
        error_code = -1
        return
    end if

    if (size(nuclear_coordinates, 1) /= 3) then
        error_code = -1
        return
    end if

    call external_potential_integrals(integrals, pot_deriv_order, multipoles, multipole_origins, &
                                      min_multipole_order, max_multipole_order, &
                                      nuclear_coordinates, nuclear_deriv_order, error_code)

    if (error_code /= 0) then
        return
    end if

end subroutine potential_integrals

end module frame_callback_procedures
