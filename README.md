FraME (Fragment-based Multiscale Embedding)
-------------------------------------------

FraME  is a software library that provides versatile embedding capability to quantum chemistry codes.

Copyright (C) 2018 Jógvan Magnus Haugaard Olsen, Karen Oda Hjorth Dundas, and Magnus Ringholm.

FraME is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

FraME is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with FraME.  If not, see <https://www.gnu.org/licenses/>.

Contact details:
Jógvan Magnus Haugaard Olsen (foeroyingur@gmail.com)

