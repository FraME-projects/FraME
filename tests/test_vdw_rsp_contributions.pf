!
!    FraME (Fragment-based Multiscale Embedding) is a software library that
!    provides versatile embedding capability to quantum chemistry codes.
!
!    Copyright (C) 2018 Jógvan Magnus Haugaard Olsen,
!                       Karen Oda Hjorth Dundas, and
!                       Magnus Ringholm.
!
!    This file is part of FraME.
!
!    FraME is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published
!    by the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    FraME is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with FraME.  If not, see <https://www.gnu.org/licenses/>.
!
!    Contact details:
!    Jógvan Magnus Haugaard Olsen (foeroyingur@gmail.com)
!

! Module for testing vdw_rsp_contributions
module test_vdw_rsp_contributions

    use pfunit_mod
    use vdw_rsp_contributions, only: vdw_rsp_expval
    use frame, only: frame_type
    use frame_tensors, only: initialize_frame_tensors, &
                             terminate_frame_tensors
    use frame_parameters, only: dp, ip

    implicit none

    real(dp), parameter :: TOLERANCE = 1.0e-12_dp

    type :: pert_case
        integer(ip), allocatable, dimension(:) :: perturbations
        real(dp), allocatable, dimension(:) :: energies
    end type pert_case

    type :: system_type
        type(frame_type) :: frame
        type(pert_case) :: g, gg
    end type system_type

    @testcase
    type, extends(testcase) :: test_frame_vdw_rsp_contributions
        type(system_type) :: Ne, HF
    contains
        procedure :: setup
        procedure :: teardown
    end type test_frame_vdw_rsp_contributions

contains

subroutine setup(this)

    class(test_frame_vdw_rsp_contributions), intent(inout) :: this
    integer :: i, j

    call initialize_frame_tensors()

    ! System
    ! Ne case
    ! g case
    this%Ne%frame%vdw_method = 1
    this%Ne%frame%combination_rule = 1

    this%Ne%frame%num_fragments = 1
    allocate(this%Ne%frame%fragments(1))

    this%Ne%frame%fragments(1)%has_vdw = .true.

    this%Ne%frame%fragments(1)%num_sites = 1
    allocate(this%Ne%frame%fragments(1)%sites(1))
    allocate(this%Ne%frame%fragments(1)%sites(1)%coordinate(3))
    this%Ne%frame%fragments(1)%sites(1)%coordinate = [5.2912331499820509_dp, 0.0_dp, 0.0_dp]

    do i = 1, this%Ne%frame%fragments(1)%num_sites
        this%Ne%frame%fragments(1)%sites(i)%lj_sigma = real(i + 3)
        this%Ne%frame%fragments(1)%sites(i)%lj_epsilon = real(i + 3)
        this%Ne%frame%fragments(1)%sites(i)%has_vdw = .true.
    end do

    this%Ne%frame%core_fragment%num_nuclei = 1
    allocate(this%Ne%frame%core_fragment%nuclei(1))

    allocate(this%Ne%frame%core_fragment%nuclei(1)%coordinate(3))
    this%Ne%frame%core_fragment%nuclei(1)%coordinate = [3.2125344276661281_dp, 0.0_dp, 0.0_dp]

    do i = 1, this%Ne%frame%core_fragment%num_nuclei
        this%Ne%frame%core_fragment%nuclei(i)%lj_sigma = real(i)
        this%Ne%frame%core_fragment%nuclei(i)%lj_epsilon = real(i)
        this%Ne%frame%core_fragment%nuclei(i)%has_vdw = .true.
    end do

    ! HF case
    this%HF%frame%vdw_method = 1
    this%HF%frame%combination_rule = 1

    this%HF%frame%num_fragments = 6
    allocate(this%HF%frame%fragments(6))

    do i = 1, 6
        this%HF%frame%fragments(i)%num_sites = 3
        allocate(this%HF%frame%fragments(i)%sites(3))

        this%HF%frame%fragments(i)%has_vdw = .true.

        do j = 1, 3
            allocate(this%HF%frame%fragments(i)%sites(j)%coordinate(3))

            this%HF%frame%fragments(i)%sites(j)%lj_sigma = real(i + 3)
            this%HF%frame%fragments(i)%sites(j)%lj_epsilon = real(i + 3)
            this%HF%frame%fragments(i)%sites(j)%has_vdw = .true.

        end do
    end do

    ! coordinate
    this%HF%frame%fragments(1)%sites(1)%coordinate = [-1.6194952891195065_dp, -4.0213371939863594_dp, &
                                                      3.7624447148622373_dp]
    this%HF%frame%fragments(1)%sites(2)%coordinate = [-0.15684726837446797_dp, -2.9234063153650833_dp, &
                                                      4.1573974749858973_dp]
    this%HF%frame%fragments(1)%sites(3)%coordinate = [-1.3606028099953846_dp, -4.5202248909846663_dp, &
                                                      1.9766535267432950_dp]
    this%HF%frame%fragments(2)%sites(1)%coordinate = [-1.2982418478705962_dp, 5.1400550599825641_dp, &
                                                      -2.4490850579916925_dp]
    this%HF%frame%fragments(2)%sites(2)%coordinate = [1.5117808999948718E-002_dp, &
                                                      4.9491927213582123_dp, -1.1281664966211731_dp]
    this%HF%frame%fragments(2)%sites(3)%coordinate = [-1.2434397902457821_dp, 3.5300084014880260_dp, &
                                                      -3.4015070249884616_dp]
    this%HF%frame%fragments(3)%sites(1)%coordinate = [-5.7844516686053780_dp, 0.10015548462466026_dp, &
                                                      -1.0469082732464488_dp]
    this%HF%frame%fragments(3)%sites(2)%coordinate = [-5.0002153267330378_dp, -1.1130486876212242_dp, &
                                                      0.14361918549951280_dp]
    this%HF%frame%fragments(3)%sites(3)%coordinate = [-5.3252482202319360_dp, 1.7820117358689551_dp, &
                                                      -0.37038632049874359_dp]
    this%HF%frame%fragments(4)%sites(1)%coordinate = [-1.6610692638693654_dp, -3.5999282681127887_dp, &
                                                      -4.1290515831109937_dp]
    this%HF%frame%fragments(4)%sites(2)%coordinate = [-0.11527329362460897_dp, -4.0081091111114038_dp, &
                                                      -3.1558426287392947_dp]
    this%HF%frame%fragments(4)%sites(3)%coordinate = [-1.4891041864949488_dp, -1.7820117358689551_dp, &
                                                      -4.5391221522346026_dp]
    this%HF%frame%fragments(5)%sites(1)%coordinate = [-2.0616912023680065_dp, 3.3183590754887438_dp, &
                                                      4.1252721308610063_dp]
    this%HF%frame%fragments(5)%sites(2)%coordinate = [-0.22676713499923076_dp, 2.9555316594899743_dp, &
                                                      4.0818084299861539_dp]
    this%HF%frame%fragments(5)%sites(3)%coordinate = [-2.8704939838652628_dp, 1.6327233719944616_dp, &
                                                      4.2235378893606725_dp]
    this%HF%frame%fragments(6)%sites(1)%coordinate = [9.3276881529683582_dp, -0.30613563224896156_dp, &
                                                      1.5798110404946408_dp]
    this%HF%frame%fragments(6)%sites(2)%coordinate = [8.9478532018446479_dp, -0.76722880674739746_dp, &
                                                      -0.19464179087433972_dp]
    this%HF%frame%fragments(6)%sites(3)%coordinate = [8.5207750975960970_dp, 1.3662719883703653_dp, &
                                                      1.8122473538688524_dp]

    this%HF%frame%core_fragment%num_nuclei = 2
    allocate(this%HF%frame%core_fragment%nuclei(2))

    do i = 1, 2
        allocate(this%HF%frame%core_fragment%nuclei(i)%coordinate(3))
        this%HF%frame%core_fragment%nuclei(i)%lj_sigma = real(i)
        this%HF%frame%core_fragment%nuclei(i)%lj_epsilon = real(i)
        this%HF%frame%core_fragment%nuclei(i)%has_vdw = .true.
    end do

    this%HF%frame%core_fragment%nuclei(1)%coordinate = [1.7_dp, 0.0_dp, 0.0_dp]
    this%HF%frame%core_fragment%nuclei(2)%coordinate = 0.0_dp

    ! Values
    ! Ne case
    ! g case
    allocate(this%Ne%g%perturbations(1))
    this%Ne%g%perturbations = [1]

    allocate(this%Ne%g%energies(3))
    this%Ne%g%energies = [-2.3142853266046650_dp, 0.0_dp, 0.0_dp]

    ! gg case
    allocate(this%Ne%gg%perturbations(2))
    this%Ne%gg%perturbations = [1, 1]

    allocate(this%Ne%gg%energies(6))
    this%Ne%gg%energies = [-2.2266673873992375_dp, 0.0_dp, 0.0_dp, 1.1133336936996188_dp, 0.0_dp, &
                            1.1133336936996188_dp]

    ! HF case
    ! g case
    allocate(this%HF%g%perturbations(1))
    this%HF%g%perturbations = [1]

    allocate(this%HF%g%energies(6))
    this%HF%g%energies = [-7.9633198843838057E-003_dp, -3.2000755314908298E-003_dp, 1.3326408225571643E-002_dp, &
                         7.0172783026710480E-003_dp, -2.2907919777623120E-003_dp, 1.4086832060488824E-003_dp]

    ! gg case
    allocate(this%HF%gg%perturbations(2))
    this%HF%gg%perturbations = [1, 1]

    allocate(this%HF%gg%energies(21))
    this%HF%gg%energies = [-3.4998303927968960E-002_dp, 9.5791308147619592E-003_dp, -1.1925832363660176E-003_dp, &
                         0.0_dp, 0.0_dp, 0.0_dp, 1.6947943104082257E-002_dp, 4.6678634905608120E-003_dp, &
                         0.0_dp, 0.0_dp, 0.0_dp, 1.8050360823886634E-002_dp, 0.0_dp, 0.0_dp, 0.0_dp, &
                         -4.2577332248156154E-003_dp, 1.1465409747293625E-003_dp, -3.9303257581591380E-004_dp, &
                         9.8884524572834731E-004_dp, -1.1312073154140872E-003_dp, 3.2688879790872577E-003_dp]

end subroutine setup

subroutine teardown(this)

    class(test_frame_vdw_rsp_contributions), intent(inout) :: this
    integer(ip) :: i, j

    call terminate_frame_tensors()

    ! System
    ! Ne case
    deallocate(this%Ne%frame%fragments(1)%sites(1)%coordinate)
    deallocate(this%Ne%frame%fragments(1)%sites)
    deallocate(this%Ne%frame%fragments)

    deallocate(this%Ne%frame%core_fragment%nuclei(1)%coordinate)
    deallocate(this%Ne%frame%core_fragment%nuclei)

    ! HF case

    do i = 1, 6
        do j = 1, 3
            deallocate(this%HF%frame%fragments(i)%sites(j)%coordinate)
        end do
        deallocate(this%HF%frame%fragments(i)%sites)
    end do

    deallocate(this%HF%frame%fragments)

    do i = 1, 2
        deallocate(this%HF%frame%core_fragment%nuclei(i)%coordinate)
    end do
    deallocate(this%HF%frame%core_fragment%nuclei)

    ! Values
    ! Ne case
    ! g case
    deallocate(this%Ne%g%perturbations)
    deallocate(this%Ne%g%energies)

    ! gg case
    deallocate(this%Ne%gg%perturbations)
    deallocate(this%Ne%gg%energies)

    ! HF case
    ! g case
    deallocate(this%HF%g%perturbations)
    deallocate(this%HF%g%energies)

    ! gg case
    deallocate(this%HF%gg%perturbations)
    deallocate(this%HF%gg%energies)

end subroutine teardown


@test
subroutine test_vdw_rsp_expval(this)

    class(test_frame_vdw_rsp_contributions), intent(inout) :: this
    real(dp), allocatable, dimension(:) :: energies
    real(dp), allocatable, dimension(:) :: ref_energies
    integer(ip) :: error_code

    ! g Ne case
    allocate(ref_energies(3))
    ref_energies = [3.53044900552509262E+02_dp, 0.0_dp, 0.0_dp]

    allocate(energies(3))
    energies = 0.0_dp

    call vdw_rsp_expval(this%Ne%frame, this%Ne%g%perturbations, energies, error_code)

    @assertequal(0, error_code)
    @assertequal(ref_energies, energies, TOLERANCE)

    deallocate(energies)
    deallocate(ref_energies)

    ! Test for wrong dimensions
    allocate(energies(6))

    call vdw_rsp_expval(this%Ne%frame, this%Ne%g%perturbations, energies, error_code)

    @assertequal(-1, error_code)

    ! gg Ne case
    allocate(ref_energies(6))
    ref_energies = [8.60552377202491152E+03_dp, 0.0_dp, 0.0_dp, -4.30276188601245576E+03_dp, &
                    0.0_dp, -4.30276188601245576E+03_dp]

    energies = 0.0_dp

    call vdw_rsp_expval(this%Ne%frame, this%Ne%gg%perturbations, energies, error_code)

    @assertequal(0, error_code)
    @assertequal(ref_energies, energies, TOLERANCE)

    ! g HF case
    energies = 0.0_dp

    ref_energies = [1.31734376293907429E+00_dp, -3.56940500573268149E-01_dp, -6.53362429453291482E-01_dp, &
                    1.75648436369089689E+00_dp,  1.02778078944357301E+01_dp,  1.84036346550479628E+01_dp]

    call vdw_rsp_expval(this%HF%frame, this%HF%g%perturbations, energies, error_code)

    @assertequal(0, error_code)
    @assertequal(ref_energies, energies, TOLERANCE)

    deallocate(energies)
    deallocate(ref_energies)

!     ! HF gg case
    allocate(ref_energies(21))
    ref_energies = [-2.12468439412238936E+01_dp, -5.76899222690526159E+00_dp, -1.25259353888838412E+01_dp, &
                     0.00000000000000000E+00_dp,  0.00000000000000000E+00_dp,  0.00000000000000000E+00_dp, &
                     8.73041790375210702E+00_dp,  2.91716329825409160E+00_dp,  0.00000000000000000E+00_dp, &
                     0.00000000000000000E+00_dp,  0.00000000000000000E+00_dp,  1.25164260374717760E+01_dp, &
                     0.00000000000000000E+00_dp,  0.00000000000000000E+00_dp,  0.00000000000000000E+00_dp, &
                    -4.15389955587255031E+02_dp, -8.20572226490476169E+01_dp, -1.73852776711922672E+02_dp, &
                    -6.49811167079924985E+01_dp,  6.65354991382192338E+02_dp,  4.80371072295247359E+02_dp]

    allocate(energies(21))
    energies = 0.0_dp

    call vdw_rsp_expval(this%HF%frame, this%HF%gg%perturbations, energies, error_code)

    @assertequal(0, error_code)
    @assertequal(ref_energies, energies, TOLERANCE)

    deallocate(energies)
    deallocate(ref_energies)

end subroutine test_vdw_rsp_expval

end module test_vdw_rsp_contributions
